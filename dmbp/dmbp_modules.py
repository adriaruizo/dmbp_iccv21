import torch
import torch.nn as nn
import torch.nn.functional as F

### Hook fn mulitplying the derivatives of the layer gradients to compute
### the full, positive or negative linear mappings
def hook_fn(module, grad_in, grad_out):
    grad_in = grad_in[0].clone()
    neuron_w = None

    # Mulitply gradient per mask
    if(module.mode=='pos'):
        p = torch.sigmoid(module.neuron_weights)
        neuron_w = p
    elif(module.mode=='neg'):
        p = 1-torch.sigmoid(module.neuron_weights)
        neuron_w = p

    if(neuron_w is not None):
        return (grad_in*neuron_w,)
    else:
        return (grad_in,)#,None

### Class encapsulating the filter masks parameters/operations for DMBP
class DMBPFilterMask(nn.Module):
    def __init__(self):
        super(DMBPFilterMask, self).__init__()
        self.first_forward = True
        self.first_backward = True
        self.neuron_weights = nn.Parameter(torch.FloatTensor([0.0]))
        self.mode = ''
        self.register_backward_hook(hook_fn)

    # Set mode for gradient computation (pos,neg or full)
    def set_mode(self,mode):
        self.mode = mode

    def forward(self,x):
        self.num_dims = 0

        ## Initialize masks in the first forward
        if(self.first_forward):
            self.num_dims = len(x.shape)
            self.first_forward = False
            self.neuron_weights.data = torch.zeros_like(x).type_as(x)
        return x

### Custom ReLU function for DMBP
class DMBPReLU(nn.Module):
    def __init__(self):
        super(DMBPReLU, self).__init__()
        self.nw_module = DMBPFilterMask()
        self.relu = nn.ReLU(inplace=False)
        self.input_var = None

    def forward(self, x):
        self.input_var = self.relu(x)
        out = self.nw_module(self.input_var)
        return out
