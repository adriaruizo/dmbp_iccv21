import copy
import torch
import torch.nn as nn
from dmbp.dmbp_modules import *
import numpy as np

# Convert a dictionary of pytorch tensors to numpy
def results_to_numpy(results):

    def tensor_to_numpy(tensor):
        if(len(tensor.shape)==3):
            tensor = tensor.detach().cpu().permute(1,2,0).numpy()
        elif(len(tensor.shape)==4):
            tensor = tensor.squeeze().detach().cpu().permute(1,2,0).numpy()
        elif(len(tensor.shape)==2):
            tensor = tensor.detach().cpu().numpy()
        return tensor

    for res_id in results.keys():
        results[res_id] = tensor_to_numpy(results[res_id])
    return results


class DMBP():
    def __init__(self, model):
        self.or_model = model
        self.model = copy.deepcopy(model)
        # Init EviDis modules
        self.init_modules()

    ### Modify ReLU layers for all the network
    def init_modules(self):
        # Function to replace modules by name and register them
        def set_new_module(model,module_name,new_module):
            hierarchy_list = module_name.split(".")
            parent_module = self.model
            if(len(hierarchy_list)==1):
                parent_module = self.model
            else:
                for name in hierarchy_list[0:-1:]:
                    if(name.isdigit()):
                        parent_module = parent_module[int(name)]
                    else:
                        parent_module = parent_module._modules[name]

            name = hierarchy_list[-1]
            if(name.isdigit()):
                parent_module[int(name)] = new_module
            else:
                parent_module._modules[name] = new_module

        for module_name,m in self.model.named_modules():
            # Use filter mask after each ReLU layer in the original network
            if(isinstance(m, nn.ReLU)):
                 new_relu = DMBPReLU()
                 set_new_module(self.model, module_name, new_relu)

    # Set backward gradient. Used to compute normal, positive
    # or negative linear mappings with backpropagation.
    def set_backward_mode(self,mode):
        for module in self.model.modules():
            if( isinstance(module, DMBPFilterMask)):
                module.set_mode(mode)
        #module.set_mode('')

    # Get filter masks parameters
    def get_neuron_weights(self):
        neuron_weights = []
        for module in self.model.modules():
            if( isinstance(module, DMBPFilterMask)):
                neuron_weights.append(module.neuron_weights)
        return neuron_weights

    # Reset internal variables
    def reset_first_forward(self):
        for module in self.model.modules():
            if( isinstance(module, DMBPFilterMask)):
                module.first_forward = True
                module.first_backward = True
                module.mode = ''

    # Get intermediate layer activations
    def get_intermidiate_vars(self):
        int_vars = []
        for module in self.model.modules():
            if( isinstance(module, DMBPReLU)):
                if(module.input_var is not None):
                    int_vars.append(module.input_var)
        return int_vars

    # Get bias terms in the network
    def get_bias_vars(self):
        bias_vars = []
        for module in self.model.modules():
            if(isinstance(module, nn.Conv2d) or
               isinstance(module, nn.Linear)):
                bias_vars.append(module.bias)
        return bias_vars


    ### Initialization function for filter masks
    def init_filter_masks(self,x, target):
        x.requires_grad = True
        y = self.model(x)
        y = self.model(x)
        y_pos = y[:,target]
        y_neg= y_pos
        int_vars = self.get_intermidiate_vars()
        neuron_weights = self.get_neuron_weights()
        int_vars = int_vars#[::-1]
        neuron_weights = neuron_weights#[::-1]

        for int_var,nw in zip(int_vars,neuron_weights):
            self.set_backward_mode('')

            ### Compute classifier positive evidences
            self.set_backward_mode('pos')
            grad_pos = torch.autograd.grad(y_pos,int_var,allow_unused=True,
                                           create_graph=False,retain_graph=True)[0]

            ### Compute classifier negative evidences
            self.set_backward_mode('neg')
            grad_neg = torch.autograd.grad(y_neg,int_var,allow_unused=True,
                                           create_graph=False,retain_graph=True)[0]

            # Initialize paramters of the filter mask
            nw.data[(grad_pos>0) & (grad_neg>0)] = 2.0
            nw.data[(grad_pos<0) & (grad_neg<0)] = -2.0
            self.set_backward_mode('')


    # Optimize filter masks to obtain the attribution map and linear mappings for a
    # given image and target label
    def attributions(self, x, target, num_its=200):
        print('---Initializing Variables---')
        self.init_filter_masks(x,target)

        print('---Optimizing Filter Masks---')
        x.requires_grad = True
        y = self.model(x)


        ### Optimization
        neuron_weights = self.get_neuron_weights()
        optimizer = torch.optim.RMSprop(neuron_weights, lr=0.01)
        y_pos = y[:,target]
        y_neg= y_pos

        # Get input variables (image and biases)
        bias_vars = []
        bias_vars = self.get_bias_vars()
        optimized_vars = [x] + bias_vars

        # Compute standard gradient
        grad_all = torch.autograd.grad(y_pos,optimized_vars,allow_unused=True,
                                       create_graph=True,retain_graph=True)

        for n in range(0,num_its):
            ### Compute  positive linear mapping
            self.set_backward_mode('pos')
            grad_pos = torch.autograd.grad(y_pos,optimized_vars,allow_unused=True,
                                           create_graph=True,retain_graph=True)

            ### Compute negative linear mapping
            self.set_backward_mode('neg')
            grad_neg = torch.autograd.grad(y_neg,optimized_vars,allow_unused=True,
                                           create_graph=True,retain_graph=True)

            self.set_backward_mode('')
            response_res = 0
            response_pos = 0
            response_neg = 0

            loss =0
            # Compute positive and negative scores for each input variable
            for gpos, gneg, gall, var in zip(grad_pos,grad_neg,grad_all,optimized_vars):
                gresidual = gall.detach() - gpos - gneg
                a = (gresidual*var.detach()).sum()
                b = (gpos*var.detach()).sum()
                c = (gneg*var.detach()).sum()
                response_res += a
                response_pos += b
                response_neg += c


            # Loss Function
            # Ensure balance between positive and negative scores
            if(response_neg.abs()*0.1>response_pos):
                loss = -response_pos #+ response_res.abs()
            elif(response_pos*0.1>response_neg.abs()):
                loss = response_neg #+ response_res.abs()
            else:
                loss = response_neg  - response_pos + response_res.abs()

            ## Gradient descent step
            optimizer.zero_grad()
            loss.backward(retain_graph=True)

            optimizer.step()
            if(n%50==0 or n==(num_its-1)):
                print('It ' + str(n) + '/' + str(num_its-1) + ' | Neg. Score: ' + "{:.2f}".format(response_neg) +
                      ' | Pos. Score: ' + "{:.2f}".format(response_pos) +
                      ' | Resid. Score: ' + "{:.2f}".format(response_res))

        ### Reset vars for next images
        self.reset_first_forward()

        ### Get positive and negative mappings
        results = {}
        grad_pos[0][grad_pos[0]*x<0] = 0
        grad_neg[0][grad_neg[0]*x>0] = 0
        results['pos_mapping'] = grad_pos[0]
        results['neg_mapping'] = grad_neg[0]

        ### Get Positive and negative attributions
        results['pos_attribution'] = (grad_pos[0]*x).squeeze()
        results['neg_attribution'] = (grad_neg[0]*x).squeeze()

        results['image'] = x
        # return results
        return results_to_numpy(results)
