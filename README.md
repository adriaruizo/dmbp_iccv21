

# Attribution Map Generation with Disentangled Masked Backpropagation

This repository contains the code (PyTorch) of the Disentangled Masked Backpropagation method to generate attribution maps as described in
["*Attribution Map Generation with Disentangled Masked Backpropagation*"](https://www.arxiv.com/) accepted for publication at [ICCV2021](http://iccv2021.thecvf.com/home).


## Contents

1. [Abstract](#abstract)
2. [Setup and Dependencies](#Setup and Dependencies)
3. [Usage](#Usage)

## Abstract

Attribution map visualization has arisen as one of the most effective techniques to understand the underlying inference process of Convolutional Neural Networks. In this task, the goal is to compute an score for each image pixel related with its contribution to the final network output. In this paper, we introduce Disentangled Masked Backpropagation (DMBP), a novel gradient-based method that leverages on the piecewise linear nature of ReLU networks to decompose the model function into different linear mappings. This decomposition aims to disentangle the positive, negative and nuisance factors from the attribution maps by learning a set of variables masking the contribution of each filter during back-propagation. A thorough evaluation over standard architectures (ResNet50 and VGG16) and benchmark datasets (PASCAL VOC and ImageNet)  demonstrates that DMBP generates more visually interpretable attribution maps than previous approaches. Additionally, we quantitatively show that the maps produced by our method are more consistent with the true contribution of each pixel to the final network output.

### Overview
<img src="Overview.jpg" width="100%">
<sub>
Overview of Disentangled Masked Backpropagation for generating attribution maps.} (Top) Given a function F(x) modelled by a CNN with ReLU non-linearities, the network output for a given image can be computed by applying a linear mapping over the input. This mapping is equivalent to the output gradients \wrt the input. Then, the attribution map indicating the contribution of each pixel can be computed as an element-wise multiplication of the mapping and the image pixels. (Middle and Bottom) DMBP learns a set of variables weighting the contribution of each network filter during back-propagation. The optimization of these variables is guided by a loss which decomposes the original function into different linear mappings disentangling positive and negative attributions.
</sub>

## Setup and Dependencies

- [Python3](https://www.python.org/downloads/)
```
conda create -n dmbp python=3.7 anaconda
conda activate dmbp
```
- [PyTorch](http://pytorch.org)
```
conda install pytorch torchvision cudatoolkit=10.2 -c pytorch
```

## Generating attribution maps
We provide an example script to generate attribution maps with DMBP. Available models are ResNet50 (trained on ImageNet) and VGG16 (trained on Pascal VOC).
Our code also provides the necessary functions to visualize the attribution maps and the positive and linear mappings obtained with DMBP.
Results are saved in '.png' format in the results folder.

### ResNet50:
Our script allows to choose an image and target label to generate the attribution map. The target label should correspond to the index of the class in each dataset. For VOC and ImageNet indeces, check "imagenet_classes.txt" and "voc_classes.txt" in the "loader" folder.

##### Target Label: Library
```
python dmbp_example.py -m resnet50 -i examples/library.jpg -tl 624
```
<img src="results/library_resnet50_class_library_dmbp.png" align="center">

##### Target Label: Church
```
python dmbp_example.py -m resnet50 -i examples/library.jpg -tl 497
```
<img src="results/library_resnet50_class_church, church building_dmbp.png" align="center">


##### Target Label: Golden Retriever
```
python dmbp_example.py -m resnet50 -i examples/golden_cat.jpg -tl 207
```
<img src="results/golden_cat_resnet50_class_golden retriever_dmbp.png" align="center">

##### Target Label: Tabby Cat
```
python dmbp_example.py -m resnet50 -i examples/golden_cat.jpg -tl 281
```
<img src="results/golden_cat_resnet50_class_tabby, tabby cat_dmbp.png" align="center">



### VGG16:

##### Target Label: Train
```
python dmbp_example.py -m vgg16 -i examples/cow_train.jpg -tl 18
```
<img src="results/cow_train_vgg16_class_train_dmbp.png" align="center">

##### Target Label: Cow
```
python dmbp_example.py -m vgg16 -i examples/cow_train.jpg -tl 9
```
<img src="results/cow_train_vgg16_class_cow_dmbp.png" align="center">

##### Target Label: Person
```
python dmbp_example.py -m vgg16 -i examples/bird_girl.jpg -tl 14
```
<img src="results/bird_girl_vgg16_class_person_dmbp.png" align="center">

##### Target Label: Bird
```
python dmbp_example.py -m vgg16 -i examples/bird_girl.jpg -tl 2
```
<img src="results/bird_girl_vgg16_class_bird_dmbp.png" align="center">
