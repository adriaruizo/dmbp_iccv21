import torch.nn as nn
from torchvision.models import resnet as resnet_modules

class Net(nn.Module):
    def __init__(self, features, classifer):
        super(Net, self).__init__()
        self.features = features
        self.pool = nn.AdaptiveAvgPool2d(1)
        self.classifier = classifer

    def forward(self, x):
        out = self.features(x)
        out = self.pool(out)
        out = out.view(out.size(0), -1)
        return self.classifier(out)


def convert_resnet_family(model):
    """
    This function wraps any (se)resnet model from torchvision or from pretrained models
    :param model: nn.Sequential
    :return: nn.Sequential
    """

    features = list()
    layer0 = nn.Sequential(
        model.conv1,
        model.bn1,
        model.relu,
        model.maxpool
    )
    features.append(layer0)

    for ind in range(1, 5):
        modules_layer = model._modules[f'layer{ind}']._modules
        new_modules = []
        for block_name in modules_layer:
            b = modules_layer[block_name]
            if isinstance(b, resnet_modules.BasicBlock):
                b = BasicResnetBlock(b)
            elif isinstance(b, resnet_modules.Bottleneck):
                b = BottleneckResnetBlock(b)
            new_modules.append(b)
        features.append(nn.Sequential(*new_modules))

    features = nn.Sequential(*features)
    classifier = model.fc
    return Net(features, classifier)


class BasicResnetBlock(nn.Module):
    expansion = 1

    def __init__(self, source_block):
        super(BasicResnetBlock, self).__init__()
        self.block1 = nn.Sequential(
            source_block.conv1,
            source_block.bn1
        )

        self.block2 = nn.Sequential(
            source_block.conv2,
            source_block.bn2
        )

        self.downsample = source_block.downsample
        self.stride = source_block.stride
        self.relu1 = nn.ReLU(inplace=True)
        self.relu2 = nn.ReLU(inplace=True)

    def forward(self, x):
        residual = x

        out = self.relu1(self.block1(x))
        out = self.block2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu2(out)

        return out


class BottleneckResnetBlock(nn.Module):
    expansion = 4

    def __init__(self, source_block):
        super(BottleneckResnetBlock, self).__init__()
        self.block1 = nn.Sequential(
            source_block.conv1,
            source_block.bn1,
        )

        self.block2 = nn.Sequential(
            source_block.conv2,
            source_block.bn2
        )

        self.block3 = nn.Sequential(
            source_block.conv3,
            source_block.bn3
        )
        self.relu1 = nn.ReLU(inplace=True)
        self.relu2 = nn.ReLU(inplace=True)
        self.relu3 = nn.ReLU(inplace=True)

        self.downsample = source_block.downsample
        self.stride = source_block.stride

    def forward(self, x):
        residual = x

        out = self.relu1(self.block1(x))
        out = self.relu2(self.block2(out))
        out = self.block3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        if(type(out) is tuple):
            out = (out[0]+residual[0],(out[1]+residual[1])*0.5)
        else:
            out += residual
        out = self.relu3(out)
        return out
