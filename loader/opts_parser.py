import argparse
import os

### Option parser
def parse_opts(parser, args_str=None):
    #### Dataset
    parser.add_argument('-i','--image_path', type=str, default='examples/motorbike_person.jpg',
                        help='Image Path (default: examples/dog.jpg)')
    parser.add_argument('-m','--model', type=str, default='vgg16',
                        help='Base Model (Default: vgg16 (vgg16,resnet50))' )
    parser.add_argument('-tl','--target_label', type=int, default=-1,
                        help='Target label for attribution map (default: class with max probability)')
    parser.add_argument('-cm','--compute_metrics', type=int, default=1,
                        help='Compute and visualize metrics (default: 1)')
    if args_str is None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(args_str)
    return args
