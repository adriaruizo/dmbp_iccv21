###
### Code for loading VGG16 model trained on VOC dataset. Adapted from torchray library (https://github.com/facebookresearch/TorchRay
###
import os
import copy
import types
import re

from collections import OrderedDict

import torch
import torch.nn as nn
from torchvision import models, transforms

###
def _fix_caffe_maxpool(model):
    for module in model.modules():
        if isinstance(module, torch.nn.MaxPool2d):
            module.ceil_mode = True

###
def _load_caffe_vgg16(model, checkpoint):
    def filt(key, value):
        # Rename some parameters to allow for the dropout layers,
        # which are not in the original checkpointed data.
        remap = {
            'classifier.0.weight': 'classifier.0.weight',
            'classifier.0.bias': 'classifier.0.bias',
            'classifier.2.weight': 'classifier.3.weight',
            'classifier.2.bias': 'classifier.3.bias',
            'classifier.4.weight': 'classifier.6.weight',
            'classifier.4.bias': 'classifier.6.bias',
        }
        key = remap.get(key, key)

        # Reshape the classifier weights.
        if key == 'features.0.weight':
            # BGR -> RGB
            value = value[:, [2, 1, 0], :, :]
        elif 'classifier' in key and 'weight' in key:
            value = value.reshape(value.shape[0], -1)
        return key, value

    checkpoint = {k: v for k, v in [
        filt(k, v) for k, v in checkpoint.items()]}

    model.load_state_dict(checkpoint)
    _fix_caffe_maxpool(model)

###
def _replace_module(curr_module, module_path, new_module):
    r"""Recursive helper function used in :func:`replace_module`.
    Args:
        curr_module (:class:`torch.nn.Module`): current module in which
            to search for the module with the relative path given by
            ``module_path``.
        module_path (list of str): path of module to replace in the model as
            a list, where each element is a member of the previous element's
            module. For example, ``'features.11'`` in AlexNet (given by
            :func:`torchvision.models.alexnet.alexnet`) refers to the 11th
            module in the ``'features'`` module, that is, the
            :class:`torch.nn.ReLU` module after the last conv layer in
            AlexNet.
        new_module (:class:`torch.nn.Module`): replacement module.
    """

    # TODO(ruthfong): Extend support to nn.ModuleList and nn.ModuleDict.
    if isinstance(curr_module, nn.Sequential):
        module_dict = OrderedDict(curr_module.named_children())
        assert module_path[0] in module_dict
        if len(module_path) == 1:
            submodule = new_module
        else:
            submodule = _replace_module(
                module_dict[module_path[0]],
                module_path[1:], new_module)
        if module_dict[module_path[0]] is not submodule:
            module_dict[module_path[0]] = submodule
            curr_module = nn.Sequential(module_dict)
    else:
        assert hasattr(curr_module, module_path[0])
        if len(module_path) == 1:
            submodule = new_module
        else:
            submodule = _replace_module(
                getattr(curr_module, module_path[0]),
                module_path[1:], new_module)
        setattr(curr_module, module_path[0], submodule)

    return curr_module

###
def replace_module(model, module_name, new_module):
    r"""Replace a :class:`torch.nn.Module` with another one in a model.
    Args:
        model (:class:`torch.nn.Module`): model in which to find and replace
            the module with the name :attr:`module_name` with
            :attr:`new_module`.
        module_name (str): path of module to replace in the model as a string,
            with ``'.'`` denoting membership in another module. For example,
            ``'features.11'`` in AlexNet (given by
            :func:`torchvision.models.alexnet.alexnet`) refers to the 11th
            module in the ``'features'`` module, that is, the
            :class:`torch.nn.ReLU` module after the last conv layer in
            AlexNet.
        new_module (:class:`torch.nn.Module`): replacement module.
    """
    return _replace_module(model, module_name.split('.'), new_module)

###
def _caffe_vgg16_to_fc(model):
    # Make shallow copy.
    model_ = copy.copy(model)

    # Transform the fully-connected layers into convolutional ones.
    for i, layer in enumerate(model.classifier.children()):
        if isinstance(layer, nn.Linear):
            out_ch, in_ch = layer.weight.shape
            k_size = 1
            if i == 0:
                in_ch = 512
                k_size = 7
            conv = nn.Conv2d(in_ch, out_ch, (k_size, k_size))
            conv.weight.data.copy_(layer.weight.view(conv.weight.shape))
            conv.bias.data.copy_(layer.bias.view(conv.bias.shape))
            model_.classifier[i] = conv

    def forward(self, x):
        # PyTorch uses a 7x7 adaptive pooling layer to feed the first
        # FC layer; here we skip it for fully-conv operation.
        x = self.features(x)
        x = self.classifier(x)
        return x

    model_.forward = types.MethodType(forward, model_)
    return model_



def get_vgg16_voc():
    r"""
    Return vgg16 model trained for voc dataset.
    The model is returned in evaluation mode.
    Returns:
        :class:`torch.nn.Module`: model.
    """

    # Set number of classes in dataset.
    num_classes = 20

    # Get/load the model from torchvision.
    model = models.__dict__['vgg16'](pretrained=True)

    # The torchvision models terminate in a classifier for ImageNet.
    # Replace that classifier if we target a different dataset.
    last_name, last_module = list(model.named_modules())[-1]

    # Construct new last layer.
    assert isinstance(last_module, nn.Linear)
    in_features = last_module.in_features
    bias = last_module.bias is not None
    new_layer_module = nn.Linear(in_features, num_classes, bias=bias)

    # Replace the last layer.
    model = replace_module(model, last_name, new_layer_module)

    # Load the model state dict from url.
    checkpoint = torch.load('loader/voc_vgg16.pth')

    # Apply the state dict and patch the torchvision models. the
    _load_caffe_vgg16(model, checkpoint)

    # return model
    model.eval()
    return model


