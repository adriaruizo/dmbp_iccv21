import torch
import torchvision.transforms as transforms
import torch.nn.functional as F

### Testing transform used by the VGG16 model provided in torchray
def get_vgg16_transform(size=224):
    bgr_mean = [103.939, 116.779, 123.68]
    mean = [m / 255. for m in reversed(bgr_mean)]
    std = [1 / 255.] * 3

    # Note: image should always be downsampled. If image is being
    # upsampled, then this resize function will not match the behavior
    # of skimage.transform.resize in "constant" mode
    # (torch.nn.functional.interpolate uses "edge" padding).
    def resize(x):
        if not isinstance(size, int):
            orig_height, orig_width = size
        else:
            height, width = x.shape[1:3]
            if width < height:
                orig_width = size
                orig_height = int(size * height / width)
            else:
                orig_height = size
                orig_width = int(size * width / height)
        with torch.no_grad():
            x = F.interpolate(x.unsqueeze(0), (orig_height, orig_width),
                              mode='bilinear', align_corners=False)
            x = x.squeeze(0)
        return x

    transform = transforms.Compose([
        transforms.ToTensor(),
        resize,
        transforms.Normalize(mean=mean, std=std),
    ])
    return transform

### Testing transform used by the Resnet50 model provided in torchvision
def get_resnet50_transform():
    transform = transforms.Compose([transforms.Resize(256),
                                    transforms.CenterCrop(224),
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.485, 0.456, 0.406),
                                                         (0.229, 0.224, 0.225)),])
    return transform
