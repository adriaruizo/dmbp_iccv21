import torch
from loader.fuse_bn_utils import *
from loader.bn_fusion_utils import *
from loader.vgg16_loader import get_vgg16_voc
from loader.transforms_models import *
import ast
import torchvision

### Create a dictionary for Imagenet classes
def read_classes_imagenet():
    file_path = 'loader/imagenet_classes.txt'
    file = open(file_path, "r")
    contents = file.read()
    dictionary = ast.literal_eval(contents)
    file.close()
    return dictionary
    #new_dict = dict([(value, key) for key, value in dictionary.items()]) 
    #return new_dict

### Create a dictionary for VOC classes
def read_classes_voc():
    file_path = 'loader/voc_classes.txt'
    file = open(file_path, "r")
    dictionary = {}
    idx = 0
    for line in file.read().splitlines():
        dictionary[idx] = line
        idx += 1
    return dictionary


### Load Resnet50/Imagenet or VGG16/VOC model
def init_model(model_name):
    print('----------------------------------------')
    print('Initializing Model: ' + model_name)

    ### Load different base model architectures
    if(model_name=='resnet50'):
        #model = torch.hub.load('pytorch/vision:v0.6.0', model_name, pretrained=True)
        model = torchvision.models.resnet50(pretrained=True)
        model = convert_resnet_family(model)
        model = fuse_bn_recursively(model)
        classes_dict = read_classes_imagenet()
        prob_layer = torch.nn.Softmax(dim=1)
        transform = get_resnet50_transform()

    elif(model_name=='vgg16'):
        model = get_vgg16_voc()
        model = fuse_bn_recursively(model)
        classes_dict = read_classes_voc()
        prob_layer = torch.nn.Sigmoid()
        transform = get_vgg16_transform(size=224)

    ### Store model info into dictionary
    model_dict = {}
    model_dict['model'] = model
    model_dict['classes'] = classes_dict
    model_dict['prob_layer'] = prob_layer
    model_dict['im_transform'] = transform

    return model_dict
