import loader as loader
import argparse
from PIL import Image
import matplotlib.pyplot as plt
import torch
import dmbp
import utils
import os
# from metric_funcs import *

if __name__=='__main__':
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    # Parse opts
    parser = argparse.ArgumentParser(description='Opts Parser')
    opts = loader.parse_opts(parser)

    # Load Model
    model_dict = loader.init_model(opts.model)
    model = model_dict['model']
    model.to(device)
    model.eval()

    classes_dict = model_dict['classes']
    prob_layer = model_dict['prob_layer']
    im_transform = model_dict['im_transform']

    # Attribution map ops
    im_path = opts.image_path
    target_label = opts.target_label

    # Load image and compute probability for target class
    input_image = Image.open(im_path)
    x = im_transform(input_image)
    x = x.unsqueeze(0).to(device)
    with torch.no_grad():
        y = model(x)
        prob_y = prob_layer(y)

    # Get target label
    if(target_label==-1):
        _,target_label = torch.max(y , dim=1)
        target_label = target_label.item()
    target_class = classes_dict[target_label]

    # Print target label probability
    target_label_prob = prob_y[0,target_label]
    print('Target Class: ' + target_class + " - Prob: " + str(target_label_prob.item()))

    # Compute attribution map
    print('Computing Attribution Map with DMBP...')

    # Initialize DMBP with base model
    dmbp = dmbp.DMBP(model)

    # Compute attribution results
    attribution_results = dmbp.attributions(x,target_label,num_its = 200)

    #Visualize results
    image_path = os.path.basename(opts.image_path)
    image_path = os.path.splitext(image_path)[0]
    results_filename = 'results/' + image_path + '_' + opts.model + '_class_' + target_class + '_dmbp.png'
    utils.visualize_attributions(attribution_results,target_class=target_class,
                                 imfile=results_filename)
